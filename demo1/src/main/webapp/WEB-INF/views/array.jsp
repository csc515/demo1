<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp" %>
<%@ include file="/WEB-INF/layouts/head.jsp" %>
<body id="demo-body">
	<div id="demo-main-div" class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<h1>Array</h1>
				<%-- Add out JSP to handle messages to the user --%>
				<%@ include file="/WEB-INF/layouts/banner.jsp" %>
				<%@ include file="/WEB-INF/layouts/message.jsp" %>
				
				${output}

			</div>
		</div>
	</div>
</body>
</html>