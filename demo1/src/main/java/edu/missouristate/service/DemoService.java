package edu.missouristate.service;

import org.springframework.stereotype.Service;

@Service
public class DemoService {

	public Integer add(Integer one, Integer two) {
		return one + two;
	}

	
}
