package edu.missouristate.service;

import org.springframework.stereotype.Service;

@Service
public class ArrayService {

	/**
	 * Get Element
	 * Expects a comma separated String
	 * Expects an action
	 * Return the result of the action on the string/array
	 * @param commaSeparatedString
	 * @param action
	 * @return
	 */
	public String getElement(String commaSeparatedString, String action) {
		String result = "";
		String[] str = commaSeparatedString.split(",");
		action = (action == null) ? "" : action.toLowerCase();
		
		for (String string : str) {
			System.out.println(string);
		}
		
		switch (action) {
		case "findfirst":
			result = str[0];
			break;

		default:
			break;
		}
				
		return result;
	}

	
	
}
