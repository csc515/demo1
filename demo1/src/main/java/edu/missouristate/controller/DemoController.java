package edu.missouristate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import edu.missouristate.service.DemoService;


@Controller
public class DemoController {
	
	@Autowired
	DemoService demoService;
	
	@GetMapping("/demo")
	public String getDemo(Model model) {
		model.addAttribute("title", "Demo Page");
		return "demo";
	}

	@GetMapping("/demo/testMessage")
	public String getMessageDemo(Model model) {
		model.addAttribute("title", "Demo Page!!!!!");
		model.addAttribute("messageType", "error");
		model.addAttribute("messageBody", "Where's the Beef?");
		model.addAttribute("customMessage", "Ummm Sure.");
		return "demo";
	}
	
	@GetMapping("/demo/add/{one}/{two}")
	public String getAddition(Model model, @PathVariable Integer one, @PathVariable Integer two) {
		model.addAttribute("customMessage", demoService.add(one, two));
		return "demo";
	}
	
	@GetMapping("/bootstrap")
	public String getCustomBoostrapPage() {
		return "bootstrap";
	}
	
}
