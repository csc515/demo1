package edu.missouristate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import edu.missouristate.service.ArrayService;

@Controller
public class ArrayController {

	@Autowired
	ArrayService arrayService;
	
	/**
	 * Query String
	 * http://somedomain.com/somePath?key=value&key=value
	 */
	
	@GetMapping(path="/array")
	public String getElement(Model model, String array, String action) {
		String output = arrayService.getElement(array, action);
		model.addAttribute("output", output);
		return "array";
	}
	
	
	
	
}
